// Bai 1

document.getElementById('btnKqBai1').addEventListener("click", () => {
    var s = 0;
    var n = 0;
    while (s < 10000) {
        n++;
        s = s + n;
    }
    document.querySelector('.kqBai1').innerHTML = `Số nguyên dương nhỏ nhất: ${n}`;
})


// Bai 2
document.getElementById('btnKqBai2').addEventListener("click", () => {
    var n = document.querySelector('.soN').value * 1;
    var x = document.querySelector('.soX').value * 1;
    var s = 0;
    for (var i = 1; i <= n; i++) {
        s += x ** i;
    }
    document.querySelector('.kqBai2').innerHTML = `Tổng: ${s}`;

})

// Bai 3
document.getElementById('btnKqBai3').addEventListener('click', () => {
    var n = document.querySelector('.numberN').value * 1;
    var t = 1;
    for (var i = 1; i <= n; i++) {
        t *= i;
    }
    document.querySelector('.kqBai3').innerHTML = `Tổng: ${t}`;
})

// Bai 4
document.getElementById('btnKqBai4').addEventListener('click', () => {
    var content = "";
    for (var i = 1; i <= 10; i++) {
        if (i % 2 == 0) {
            content += "<div class=\"bg-primary w-100\">Div chẳn</div>";
        }
        else {
            content += "<div class=\"bg-danger w-100\">Div lẻ</div>";
        }
    }
    document.querySelector('.kqBai4').innerHTML = content;
})




const BT_1 = document.querySelector('.BT_1');
const BT_1_Active = document.getElementById('BT-1');
const BT_2 = document.querySelector('.BT_2');
const BT_2_Active = document.getElementById('BT-2');
const BT_3 = document.querySelector('.BT_3');
const BT_3_Active = document.getElementById('BT-3');
const BT_4 = document.querySelector('.BT_4');
const BT_4_Active = document.getElementById('BT-4');


BT_1.addEventListener('click', () => {
    BT_1_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
})

BT_2.addEventListener('click', () => {
    BT_2_Active.classList.toggle('active')
    BT_1_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
})

BT_3.addEventListener('click', () => {
    BT_3_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_1_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
})

BT_4.addEventListener('click', () => {
    BT_4_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_1_Active.classList.remove('active')
})